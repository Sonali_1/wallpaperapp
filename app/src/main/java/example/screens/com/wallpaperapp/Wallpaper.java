//package example.screens.com.wallpaperapp;
//
//
//import android.Manifest;
//import android.app.Activity;
//import android.app.WallpaperManager;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.content.pm.PackageManager;
//import android.database.Cursor;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.net.Uri;
//import android.os.Bundle;
//import android.provider.MediaStore;
//import android.support.design.widget.FloatingActionButton;
//import android.support.v4.content.ContextCompat;
//import android.util.Log;
//import android.view.View;
//import android.widget.ImageView;
//import android.widget.Toast;
//
//import java.io.IOException;
//
///**
// * Created by sonali on 3/30/18.
// */
//
//public class Wallpaper extends Activity {
//    private FloatingActionButton fab;
//    private static int RESULT_LOAD_IMG = 1;
//    String imgpath,storedpath;
//    SharedPreferences sp;
//    ImageView myImage;
//    Bitmap bitmap;
  //  @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//        myImage =  findViewById(R.id.image_view);
//        fab= findViewById(R.id.fab);
//            fab.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent galleryIntent = new Intent(Intent.ACTION_PICK,
//                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                    startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
//
//                    if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_CALENDAR)
//                            != PackageManager.PERMISSION_GRANTED) {
//                        // Permission is not granted
//                    }
//
//
//                    sp=getSharedPreferences("setback", MODE_PRIVATE);
//                    if(sp.contains("imagepath")) {
//                        storedpath=sp.getString("imagepath", "");
//                        myImage.setImageBitmap(BitmapFactory.decodeFile(storedpath));
//                    }
//
//
//                    WallpaperManager wallpaperManager = WallpaperManager.getInstance(getApplicationContext());
//                    try {
//                        wallpaperManager.setBitmap(bitmap);
//                        Toast.makeText(getApplicationContext(), "Wallpaper Set", Toast.LENGTH_SHORT).show();
//
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        try {
//            // When an Image is picked
//            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK
//                    && null != data) {
//                // Get the Image from data
//
//                Uri selectedImage = data.getData();
//                String[] filePathColumn = { MediaStore.MediaColumns.DATA };
//
//                // Get the cursor
//                Cursor cursor = getContentResolver().query(selectedImage,
//                        filePathColumn, null, null, null);
//                // Move to first row
//                cursor.moveToFirst();
//
//                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//                imgpath = cursor.getString(columnIndex);
//                Log.d("path", imgpath);
//                cursor.close();
//
//                SharedPreferences.Editor edit=sp.edit();
//                edit.putString("imagepath",imgpath);
//                edit.commit();
//
////                BitmapFactory.Options options = new BitmapFactory.Options();
////                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
////                 bitmap = BitmapFactory.decodeFile(imgpath, options);
//
//                myImage.setImageBitmap(bitmap);
//            }
//            else {
//                Toast.makeText(this, "You haven't picked Image", Toast.LENGTH_LONG).show();
//            }
//        } catch (Exception e) {
//            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
//                    .show();
//        }
//    }
//}
//
//
//
//
//
//
