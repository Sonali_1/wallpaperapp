package example.screens.com.wallpaperapp;

import android.app.Activity;
import android.app.WallpaperManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.ImageView;

import java.io.IOException;

/**
 * Created by sonali on 3/31/18.
 */

public class WallpaperSample extends Activity {

    private static int LOAD_IMAGE_RESULT = 1;
    private FloatingActionButton fab;
    ImageView myImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fab = findViewById(R.id.fab);
        myImage = findViewById(R.id.image_view);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, LOAD_IMAGE_RESULT);
            }
        });
    }


        public void onActivityResult(int requestCode, int resultCode, Intent data){
            super.onActivityResult(requestCode, resultCode, data);
            if(requestCode == LOAD_IMAGE_RESULT && resultCode == RESULT_OK && data != null){
                Uri pick = data.getData();
                String[] images = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(pick,images,null,null,null);
                cursor.moveToFirst();

                String imagePath = cursor.getString(cursor.getColumnIndex(images[0]));
                myImage.setImageBitmap(BitmapFactory.decodeFile(imagePath));
                cursor.close();


                WallpaperManager wManager = WallpaperManager.getInstance(getApplicationContext());
                try {
                    wManager.setBitmap(BitmapFactory.decodeFile(imagePath));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
}
